﻿namespace PokemonWeakpointType
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.offenceType = new System.Windows.Forms.ComboBox();
            this.defenseType1 = new System.Windows.Forms.ComboBox();
            this.defenseType2 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.magnificationLabel = new System.Windows.Forms.Label();
            this.battolButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // offenceType
            // 
            this.offenceType.FormattingEnabled = true;
            this.offenceType.Location = new System.Drawing.Point(12, 41);
            this.offenceType.Name = "offenceType";
            this.offenceType.Size = new System.Drawing.Size(87, 20);
            this.offenceType.TabIndex = 0;
            this.offenceType.SelectedIndexChanged += new System.EventHandler(this.OffenceSelected);
            // 
            // defenseType1
            // 
            this.defenseType1.FormattingEnabled = true;
            this.defenseType1.Location = new System.Drawing.Point(12, 103);
            this.defenseType1.Name = "defenseType1";
            this.defenseType1.Size = new System.Drawing.Size(87, 20);
            this.defenseType1.TabIndex = 1;
            this.defenseType1.SelectedIndexChanged += new System.EventHandler(this.Defense1Selected);
            // 
            // defenseType2
            // 
            this.defenseType2.FormattingEnabled = true;
            this.defenseType2.Location = new System.Drawing.Point(12, 138);
            this.defenseType2.Name = "defenseType2";
            this.defenseType2.Size = new System.Drawing.Size(87, 20);
            this.defenseType2.TabIndex = 2;
            this.defenseType2.SelectedIndexChanged += new System.EventHandler(this.Defense2Selected);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "攻撃側タイプ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "防御側タイプ";
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.messageLabel.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.messageLabel.Location = new System.Drawing.Point(109, 25);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(216, 20);
            this.messageLabel.TabIndex = 5;
            this.messageLabel.Text = "　　　               　　　　　    ";
            this.messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(110, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "ダメージ倍率";
            // 
            // magnificationLabel
            // 
            this.magnificationLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.magnificationLabel.Font = new System.Drawing.Font("MS UI Gothic", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.magnificationLabel.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.magnificationLabel.Location = new System.Drawing.Point(105, 103);
            this.magnificationLabel.Name = "magnificationLabel";
            this.magnificationLabel.Size = new System.Drawing.Size(220, 80);
            this.magnificationLabel.TabIndex = 8;
            this.magnificationLabel.Text = "？";
            this.magnificationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.magnificationLabel.UseWaitCursor = true;
            // 
            // battolButton
            // 
            this.battolButton.Location = new System.Drawing.Point(16, 168);
            this.battolButton.Name = "battolButton";
            this.battolButton.Size = new System.Drawing.Size(75, 31);
            this.battolButton.TabIndex = 9;
            this.battolButton.Text = "たたかう";
            this.battolButton.UseVisualStyleBackColor = true;
            this.battolButton.Click += new System.EventHandler(this.BattolButtonClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(329, 211);
            this.Controls.Add(this.battolButton);
            this.Controls.Add(this.magnificationLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.defenseType2);
            this.Controls.Add(this.defenseType1);
            this.Controls.Add(this.offenceType);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox offenceType;
        private System.Windows.Forms.ComboBox defenseType1;
        private System.Windows.Forms.ComboBox defenseType2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label magnificationLabel;
        private System.Windows.Forms.Button battolButton;
    }
}

