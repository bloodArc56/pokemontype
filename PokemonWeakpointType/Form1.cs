﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokemonWeakpointType
{
    public partial class Form1 : Form
    {
        Dictionary<string, int> types = new Dictionary<string, int>();

        public Form1()
        {
            InitializeComponent();
        }

        public void LoadTypeList()
        {
            types.Add("",0);
            types.Add("ノーマル",1);
            types.Add("ほのお",2);
            types.Add("みず",3);
            types.Add("でんき",4);
            types.Add("くさ",5);
            types.Add("こおり",6);
            types.Add("かくとう",7);
            types.Add("どく",8);
            types.Add("じめん",9);
            types.Add("ひこう",10);
            types.Add("エスパー",11);
            types.Add("むし",12);
            types.Add("いわ", 13);
            types.Add("ゴースト", 14);
            types.Add("ドラゴン",15);
            types.Add("あく",16);
            types.Add("はがね",17);
            types.Add("フェアリー",18);

            foreach (string type in types.Keys)
            {
                this.offenceType.Items.Add(type);
                this.defenseType1.Items.Add(type);
                this.defenseType2.Items.Add(type);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadTypeList();
        }

        private void OffenceSelected(object sender, EventArgs e)
        {
            int of = this.offenceType.SelectedIndex;
        }

        private void Defense1Selected(object sender, EventArgs e)
        {
            int df1 = this.defenseType1.SelectedIndex;
        }

        private void Defense2Selected(object sender, EventArgs e)
        {
            int df2 = this.defenseType2.SelectedIndex;
        }

 
        private void BattolButtonClicked(object sender, EventArgs e)
        {
            double magnification;
            int ofTypeId;
            int dfTypeId1;
            int dfTypeId2;

            //選択欄から「現在選択中のタイプ名」を取得、
            //それをKeyとしてtypesからidを取り出し
            ofTypeId = types[this.offenceType.Text];
            dfTypeId1 = types[this.defenseType1.Text];
            dfTypeId2 = types[this.defenseType2.Text];

            //「こうげきタイプ」「ぼうぎょタイプ１」のいずれかが
            //空白の場合、処理をしない（メソッドを即終了）
            if (ofTypeId == 0 || dfTypeId1 == 0)
            {
                return;
            }
            //正常に選ばれていた場合は比較を開始
            magnification=TypeCheck.Checker(ofTypeId, dfTypeId1);
            if(dfTypeId2 != 0)
            {
            magnification = magnification *(TypeCheck.Checker(ofTypeId, dfTypeId2));
            }
            magnificationLabel.Text = magnification.ToString()+"倍";
            if(magnification　>= 2.0)
            {
                magnificationLabel.Text = magnification.ToString() + "倍";
                messageLabel.Text = "こうかはばつぐんだ";
            }
            else if (magnification == 0)
            {
                messageLabel.Text = "こうかがないみたいだ...";
            }
            else if(magnification <= 0.5)
            {
                magnificationLabel.Text = "半減";
                messageLabel.Text = "こうかはいまひとつのようだ";
                if(magnification == 0.25)
                {
                    magnificationLabel.Text = "1/4";
                }
            }
            else
            {
                magnificationLabel.Text = "等倍";
                messageLabel.Text = "";
            }
        }
    }
}
