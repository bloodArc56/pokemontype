﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonWeakpointType
{
    class TypeCheck
    {
        public static double Checker(int of, int df)
        {
            switch (of)
            {
                case 1://ノーマル
                    if(df == 13 || df == 17)//いまひとつ
                    {
                        return 0.5;
                    }
                    else if (df == 14)//こうかなし
                    {
                        return 0;
                    }
                    else//等倍
                    {
                        return 1;
                    }
                case 2://ほのお
                    if (df==5||df==6||df==12||df==17)
                    {
                        return 2;
                    }
                    else if (df==2||df==3||df==13||df==15)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 3://みず
                    if (df==2|| df==9|| df==13)
                    {
                        return 2;
                    }
                    else if (df==3||df==5||df==15)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 4://でんき
                    if (df==3||df==10)
                    {
                        return 2;
                    }
                    else if(df==4||df==5||df==15)
                    {
                        return 0.5;
                    }
                    else if(df==9)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 5://くさ
                    if (df==3||df==9||df==13)
                    {
                        return 2;
                    }
                    else if (df==2||df==5||df==8||df==10||df==12||df==15||df==17)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 6://こおり
                    if (df==5||df==9||df==10)
                    {
                        return 2;
                    }
                    else if (df==2||df==3||df==6||df==17)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 7://かくとう
                    if (df==1||df==6||df==13||df==16||df==17)
                    {
                        return 2;
                    }
                    else if (df==8||df==10||df==11||df==12||df==18)
                    {
                        return 0.5;
                    }
                    else if (df==14)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 8://どく
                    if (df==5||df==18)
                    {
                        return 2;
                    }
                    else if (df==8||df==9||df==13||df==14)
                    {
                        return 0.5;
                    }
                    else if (df==17)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 9://じめん
                    if (df==2||df==4||df==13||df== 17)
                    {
                        return 2;
                    }
                    else if (df==5||df==12)
                    {
                        return 0.5;
                    }
                    else if (df==10)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 10://ひこう
                    if (df==5||df==7||df==12)
                    {
                        return 2;
                    }
                    else if (df==4||df==13||df==17)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 11://エスパー
                    if (df==7||df==8)
                    {
                        return 2;
                    }
                    else if (df==11||df==17)
                    {
                        return 0.5;
                    }
                    else if (df == 16)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 12://むし
                    if (df==5||df==11||df==16)
                    {
                        return 2;
                    }
                    else if (df==2||df==7||df==8||df==10||df==14||df==17||df==18)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 13://いわ
                    if (df==2||df==6||df==10||df==12)
                    {
                        return 2;
                    }
                    else if (df==7||df==9||df==17)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 14://ゴースト
                    if (df==11||df==14)
                    {
                        return 2;
                    }
                    else if (df==16)
                    {
                        return 0.5;
                    }
                    else if (df == 1)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 15://ドラゴン
                    if (df==15)
                    {
                        return 2;
                    }
                    else if (df==17)
                    {
                        return 0.5;
                    }
                    else if (df==18)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                case 16://あく
                    if (df==11||df==14)
                    {
                        return 2;
                    }
                    else if (df==7||df==16||df==18)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 17://はがね
                    if (df==6||df==13||df==18)
                    {
                        return 2;
                    }
                    else if (df==2||df==3||df==4||df==17)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                case 18://フェアリー
                    if (df==7||df==15||df==16)
                    {
                        return 2;
                    }
                    else if (df==2||df==8||df==17)
                    {
                        return 0.5;
                    }
                    else
                    {
                        return 1;
                    }
                //それ以外
                default: return 0;
            }
        }

    }
}
